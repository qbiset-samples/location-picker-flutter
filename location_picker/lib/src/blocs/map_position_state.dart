/*
 * Copyright (c) 2020 Quentin BISET. All rights reserved.
 */

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapPositionState extends Equatable {
  const MapPositionState(this.coordinates);

  final LatLng coordinates;

  @override
  List<Object> get props => [coordinates];
}

class MapPositionWithAddressPending extends MapPositionState {
  MapPositionWithAddressPending({@required LatLng coordinates}): super(coordinates);
}

class MapPositionWithAddressUnavailable extends MapPositionState {
  MapPositionWithAddressUnavailable({@required LatLng coordinates}): super(coordinates);
}

class MapPositionWithAddressFound extends MapPositionState {
  const MapPositionWithAddressFound({@required LatLng coordinates, @required this.address}): super(coordinates);

  final String address;

  @override
  List<Object> get props => [coordinates, address];
}