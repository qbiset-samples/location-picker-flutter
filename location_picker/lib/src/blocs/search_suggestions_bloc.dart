/*
 * Copyright (c) 2020 Quentin BISET. All rights reserved.
 */

import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart' show LatLng;
import 'package:google_maps_webservice/places.dart';
import 'package:rxdart/rxdart.dart';
import 'package:uuid/uuid.dart';

import 'search_suggestions_event.dart';
import 'search_suggestions_state.dart';

export 'search_suggestions_event.dart';
export 'search_suggestions_state.dart';

class SearchSuggestionsBloc extends Bloc<SearchSuggestionsEvent, SearchSuggestionsState> {
  SearchSuggestionsBloc({@required this.apiKey}) : super(SearchSuggestionsHideListState());

  final String apiKey;
  final String _autocompleteToken = Uuid().v4();

// This function is used to debounce the events.
  @override
  Stream<Transition<SearchSuggestionsEvent, SearchSuggestionsState>> transformEvents(
    Stream<SearchSuggestionsEvent> events,
    TransitionFunction<SearchSuggestionsEvent, SearchSuggestionsState> transitionFn,
  ) {
    return super.transformEvents(
      events.debounceTime(const Duration(milliseconds: 800)),
      transitionFn,
    );
  }

  @override
  Stream<SearchSuggestionsState> mapEventToState(SearchSuggestionsEvent event) async* {
    if (event is UserSearchChangedEvent) {
      List<Prediction> suggestions =
          await _getSuggestions(userInput: event.userSearch, aroundCoordinates: event.aroundCoordinates);

      if (suggestions.isEmpty) {
        yield SearchSuggestionsHideListState();
      } else {
        yield SearchSuggestionsFoundState(suggestions);
      }
    } else if (event is SuggestionSelectedEvent) {
      LatLng coordinates = await _getCoordinatesForSelectedSuggestion(event.selectedSuggestion);
      yield SearchSuggestionsSelectedState(coordinates: coordinates, formattedAddress: event.selectedSuggestion.description);
    }
  }

  Future<List<Prediction>> _getSuggestions({String userInput, LatLng aroundCoordinates}) async {
    if (userInput.length == 0) {
      return [];
    }

    final placesService = GoogleMapsPlaces(apiKey: apiKey);
    final location = Location(aroundCoordinates.latitude, aroundCoordinates.longitude);
    PlacesAutocompleteResponse response = await placesService.autocomplete(userInput,
        sessionToken: _autocompleteToken, language: "fr", location: location, radius: 20000, types: ["address"], strictbounds: true, components: [Component(Component.country, "fr")]);

    if (!response.isOkay || response.predictions == null) {
      print(response.errorMessage);
      return [];
    }

    return response.predictions;
  }

  Future<LatLng> _getCoordinatesForSelectedSuggestion(Prediction suggestion) async {
    final placesService = GoogleMapsPlaces(apiKey: apiKey);
    PlacesDetailsResponse response = await placesService.getDetailsByPlaceId(suggestion.placeId, sessionToken: _autocompleteToken, language: "fr", fields: ["geometry"]);

    if (!response.isOkay || response.result == null) {
      print(response.errorMessage);
      return null;
    }

    Location location = response.result.geometry.location;
    return LatLng(location.lat, location.lng);
  }
}