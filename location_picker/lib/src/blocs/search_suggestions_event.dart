/*
 * Copyright (c) 2020 Quentin BISET. All rights reserved.
 */

import 'package:flutter/foundation.dart';
import 'package:equatable/equatable.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';

abstract class SearchSuggestionsEvent extends Equatable {
  const SearchSuggestionsEvent();

  @override
  List<Object> get props => [];
}

class UserSearchChangedEvent extends SearchSuggestionsEvent {
  const UserSearchChangedEvent(this.userSearch, {@required this.aroundCoordinates});

  final String userSearch;
  final LatLng aroundCoordinates;

  @override
  List<Object> get props => [userSearch, aroundCoordinates];
}

class SuggestionSelectedEvent extends SearchSuggestionsEvent {
  const SuggestionSelectedEvent(this.selectedSuggestion) : super();

  final Prediction selectedSuggestion;

  @override
  List<Object> get props => [selectedSuggestion];
}