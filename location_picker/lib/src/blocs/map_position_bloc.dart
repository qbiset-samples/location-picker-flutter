/*
 * Copyright (c) 2020 Quentin BISET. All rights reserved.
 */

import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/geocoding.dart';
import 'package:rxdart/rxdart.dart';

import 'map_position_event.dart';
import 'map_position_state.dart';

export 'map_position_event.dart';
export 'map_position_state.dart';

class MapPositionBloc extends Bloc<MapPositionEvent, MapPositionState> {
  MapPositionBloc({@required this.apiKey, @required LatLng initialCoordinates})
      : super(MapPositionWithAddressPending(coordinates: initialCoordinates));

  final String apiKey;

  // This function is used to debounce the events.
  @override
  Stream<Transition<MapPositionEvent, MapPositionState>> transformEvents(
    Stream<MapPositionEvent> events,
    TransitionFunction<MapPositionEvent, MapPositionState> transitionFn,
  ) {
    return super.transformEvents(
      events.debounceTime(const Duration(milliseconds: 500)),
      transitionFn,
    );
  }

  @override
  Stream<MapPositionState> mapEventToState(MapPositionEvent event) async* {
    LatLng coordinates;
    String address;

    if (event is MapPositionWithLocationEvent) {
      coordinates = event.coordinates;

      if (event is MoveCameraToSelectedLocationEvent) {
        address = event.address;
      }
    } else {
      Position userLocation =
          await getCurrentPosition(desiredAccuracy: LocationAccuracy.high, forceAndroidLocationManager: true);
      coordinates = LatLng(userLocation.latitude, userLocation.longitude);
    }

    yield MapPositionWithAddressPending(coordinates: coordinates);

    try {
      if (address == null) {
        address = await _getAddressFromLocation(coordinates);
      }
      yield MapPositionWithAddressFound(coordinates: coordinates, address: address);
    } on Exception {
      yield MapPositionWithAddressUnavailable(coordinates: coordinates);
    }
  }

  Future<String> _getAddressFromLocation(LatLng coordinates) async {
    final geocoding = new GoogleMapsGeocoding(apiKey: apiKey);
    final location = Location(coordinates.latitude, coordinates.longitude); // Location from webservice package, not model.

    GeocodingResponse response = await geocoding.searchByLocation(location, language: "fr");
    if (response.isOkay) {
      GeocodingResult place = response.results.first;
      return place.formattedAddress;
    } else {
      throw Exception(response.errorMessage);
    }
  }
}