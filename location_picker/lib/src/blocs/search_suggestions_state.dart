/*
 * Copyright (c) 2020 Quentin BISET. All rights reserved.
 */

import 'package:flutter/foundation.dart';
import 'package:equatable/equatable.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart' show LatLng;

abstract class SearchSuggestionsState extends Equatable {
  const SearchSuggestionsState({@required this.showSuggestionsList});

  final bool showSuggestionsList;

  @override
  List<Object> get props => [];
}

class SearchSuggestionsHideListState extends SearchSuggestionsState {
  const SearchSuggestionsHideListState() : super(showSuggestionsList: false);
}

class SearchSuggestionsFoundState extends SearchSuggestionsState {
  const SearchSuggestionsFoundState(this.suggestions) : super(showSuggestionsList: true);

  final List<Prediction> suggestions;

  @override
  List<Object> get props => [suggestions];
}

class SearchSuggestionsSelectedState extends SearchSuggestionsState {
  const SearchSuggestionsSelectedState({@required this.coordinates, @required this.formattedAddress}) : super(showSuggestionsList: false);

  final LatLng coordinates;
  final String formattedAddress;

  @override
  List<Object> get props => [coordinates, formattedAddress];
}