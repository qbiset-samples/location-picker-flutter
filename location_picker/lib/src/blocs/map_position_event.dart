/*
 * Copyright (c) 2020 Quentin BISET. All rights reserved.
 */

import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

abstract class MapPositionEvent extends Equatable {
  const MapPositionEvent();

  @override
  List<Object> get props => [];
}

class MoveCameraToUserLocationEvent extends MapPositionEvent {
  const MoveCameraToUserLocationEvent() : super();
}

abstract class MapPositionWithLocationEvent extends MapPositionEvent {
  const MapPositionWithLocationEvent(this.coordinates) : super();

  final LatLng coordinates;

  @override
  List<Object> get props => [coordinates];
}

class CameraMovedToLocationEvent extends MapPositionWithLocationEvent {
  const CameraMovedToLocationEvent({@required LatLng coordinates}) : super(coordinates);
}

class MoveCameraToSelectedLocationEvent extends MapPositionWithLocationEvent {
  const MoveCameraToSelectedLocationEvent({@required LatLng coordinates, @required this.address}) : super(coordinates);

  final String address;

  @override
  List<Object> get props => [coordinates, address];
}