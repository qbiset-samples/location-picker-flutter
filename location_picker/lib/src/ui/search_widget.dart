/*
 * Copyright (c) 2020 Quentin BISET. All rights reserved.
 */

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart' show LatLng;
import 'package:google_maps_webservice/places.dart';
import 'package:location_picker/src/blocs/search_suggestions_bloc.dart';

class SearchWidget extends StatefulWidget {
  SearchWidget(
      {@required this.apiKey,
      @required this.mapCoordinates,
      this.onSearchSuggestionTapped,
      this.onKeyboardAppearing,
      this.onKeyboardDisappearing})
      : super();

  final String apiKey;
  final LatLng mapCoordinates;
  final void Function(LatLng, String) onSearchSuggestionTapped;
  final void Function() onKeyboardAppearing;
  final void Function() onKeyboardDisappearing;

  @override
  _SearchWidgetState createState() => _SearchWidgetState();
}

class _SearchWidgetState extends State<SearchWidget> {
  TextEditingController _textEditingController;
  SearchSuggestionsBloc _suggestionsBloc;

  @override
  void initState() {
    super.initState();
    _textEditingController = TextEditingController();
    _suggestionsBloc = SearchSuggestionsBloc(apiKey: widget.apiKey);
  }

  @override
  void dispose() {
    _textEditingController.dispose();
    _suggestionsBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);

    return BlocConsumer(
        cubit: _suggestionsBloc,
        listener: (BuildContext context, SearchSuggestionsState state) {
          if (state is SearchSuggestionsSelectedState) {
            // Updating the text field with the selection.
            final newValue = TextEditingValue(
                text: state.formattedAddress,
                selection: TextSelection.fromPosition(TextPosition(offset: state.formattedAddress.length)));
            _textEditingController.value = newValue;
            if (FocusManager.instance.primaryFocus.hasFocus) {
              FocusManager.instance.primaryFocus.unfocus();
            }

            // Sending callback to location picker to move the map if needed.
            if (state.coordinates != null) {
              widget.onSearchSuggestionTapped(state.coordinates, state.formattedAddress);
            }
          }
        },
        builder: (BuildContext context, SearchSuggestionsState state) {
          List<Widget> children = [];

          // The text field is always visible.
          final textField = _buildTextField(context);
          children.add(textField);

          // If the state calls for it, display a small list of suggestions below the text field.
          if (state is SearchSuggestionsFoundState) {
            final positionedSuggestionsView =
                _buildContainedSuggestionsListView(context, suggestions: state.suggestions);

            children.add(positionedSuggestionsView);
          }

          return Column(children: children);
        });
  }

  Widget _buildTextField(BuildContext context) {
    ThemeData themeData = Theme.of(context);

    return FocusScope(
        key: Key("Search Text Field"),
        autofocus: false,
        child: Focus(
            onFocusChange: (bool focused) {
              if (focused) {
                widget.onKeyboardAppearing();
              } else {
                widget.onKeyboardDisappearing();
              }
            },
            child: CupertinoTextField(
                controller: _textEditingController,
                style: themeData.textTheme.bodyText1,
                textAlignVertical: TextAlignVertical.center,
                padding: EdgeInsets.only(right: 10.0),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.white70, width: 1.0),
                    borderRadius: BorderRadius.circular(30.0),
                    color: themeData.backgroundColor,
                    boxShadow: [const BoxShadow(color: Colors.grey, offset: Offset(0.0, 1.0), blurRadius: 1.0)]),
                minLines: 1,
                maxLines: 1,
                maxLengthEnforced: false,
                autocorrect: false,
                prefix: const Padding(
                    padding: EdgeInsets.all(5.0), child: Icon(Icons.search, color: Color.fromRGBO(0, 239, 209, 1.0))),
                keyboardType: TextInputType.streetAddress,
                textInputAction: TextInputAction.search,
                autofillHints: [AutofillHints.fullStreetAddress],
                clearButtonMode: OverlayVisibilityMode.editing,
                onChanged: (String newInput) {
                  final event = UserSearchChangedEvent(newInput, aroundCoordinates: widget.mapCoordinates);
                  _suggestionsBloc.add(event);
                })));
  }

  Widget _buildContainedSuggestionsListView(BuildContext context, {@required List<Prediction> suggestions}) {
    ThemeData themeData = Theme.of(context);

    return Container(
        key: Key("Suggestions List View"),
        margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
        decoration: BoxDecoration(
            border: Border.all(color: Colors.white70, width: 1.0),
            borderRadius: BorderRadius.circular(15.0),
            color: themeData.backgroundColor,
            boxShadow: [const BoxShadow(color: Colors.grey, offset: Offset(0.0, 1.0), blurRadius: 1.0)]),
        child: _SuggestionsListView(
            suggestionsList: suggestions,
            onTileTapped: (Prediction prediction) {
              final event = SuggestionSelectedEvent(prediction);
              _suggestionsBloc.add(event);
            }));
  }
}

class _SuggestionsListView extends StatelessWidget {
  _SuggestionsListView({Key key, @required this.suggestionsList, this.onTileTapped}) : super(key: key);

  final List<Prediction> suggestionsList;
  int get suggestionsCount => min(suggestionsList.length, 3);

  final void Function(Prediction) onTileTapped;

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    List<Widget> children = [];

    for (int index = 0; index < suggestionsCount; index++) {
      Prediction prediction = suggestionsList[index];

      ListTile tile = ListTile(
        key: ObjectKey(prediction),
        visualDensity: VisualDensity.compact,
        dense: true,
        contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
        leading: const Icon(Icons.location_on, color: Color.fromRGBO(0, 239, 209, 1.0)),
        title: Text(prediction.description, style: themeData.textTheme.bodyText1, maxLines: 1),
        onTap: () {
          onTileTapped(prediction);
        },
      );

      children.add(tile);

      if (index != suggestionsCount - 1) {
        children.add(const Divider(color: Colors.black38, height: 1.0));
      }
    }

    return ListBody(mainAxis: Axis.vertical, children: children);
  }
}
