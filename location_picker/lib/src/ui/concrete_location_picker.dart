/*
 * Copyright (c) 2020 Quentin BISET. All rights reserved.
 */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'package:location_picker/src/blocs/map_position_bloc.dart';
import 'package:location_picker/src/model/location.dart' as model;
import 'package:location_picker/src/ui/address_card.dart';
import 'package:location_picker/src/ui/search_widget.dart';

class LocationPicker extends StatefulWidget {
  LocationPicker(
      {Key key, @required this.apiKey, @required this.initialCoordinates, @required this.title, this.onSelected})
      : super(key: key);

  final String apiKey;
  final LatLng initialCoordinates;
  final String title;
  final void Function(model.Location) onSelected;

  @override
  _LocationPickerState createState() => _LocationPickerState();
}

class _LocationPickerState extends State<LocationPicker> {
  GoogleMapController _controller;
  MapPositionBloc _mapPositionBloc;

  LatLng _currentCameraTarget;
  LatLng get currentCameraTarget => _currentCameraTarget;

  bool _showAddressCard = true;

  @override
  void initState() {
    super.initState();

    _currentCameraTarget = widget.initialCoordinates;

    _mapPositionBloc = MapPositionBloc(apiKey: widget.apiKey, initialCoordinates: widget.initialCoordinates);
    _mapPositionBloc.add(MoveCameraToUserLocationEvent());
  }

  @override
  void dispose() {
    _controller.dispose();
    _mapPositionBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    return Scaffold(
        appBar: AppBar(title: Text(widget.title, style: themeData.textTheme.headline6, maxLines: 2), centerTitle: true),
        body: BlocConsumer<MapPositionBloc, MapPositionState>(
            cubit: _mapPositionBloc,
            listener: (context, state) {
              if (!_areCoordinatesEqual(_currentCameraTarget, state.coordinates)) {
                _moveCameraToTarget(state.coordinates);
              }
            },
            builder: (context, state) {
              List<Widget> stackChildren = [
                _buildGoogleMap(),
                Icon(Icons.location_searching, key: const Key("Pointer"), color: themeData.buttonColor),
                _buildPositionedSearchWidget()
              ];

              if (_showAddressCard) {
                stackChildren.add(_buildPositionedAddressCard(state));
              }

              return Stack(alignment: Alignment.center, fit: StackFit.loose, children: stackChildren);
            }));
  }

  Widget _buildGoogleMap() {
    return GoogleMap(
        key: const Key("Google Map"),
        initialCameraPosition: CameraPosition(target: _mapPositionBloc.state.coordinates, zoom: 16.0),
        zoomControlsEnabled: false,
        zoomGesturesEnabled: true,
        onMapCreated: (GoogleMapController controller) {
          _controller = controller;
        },
        onCameraMove: (CameraPosition position) {
          LatLng newCoordinates = position.target;
          _currentCameraTarget = newCoordinates;
          _mapPositionBloc.add(CameraMovedToLocationEvent(coordinates: newCoordinates));
        });
  }

  Widget _buildPositionedSearchWidget() {
    return Positioned(
        key: Key("Search Widget"),
        top: 15.0,
        left: 20.0,
        right: 20.0,
        child: SearchWidget(
          apiKey: widget.apiKey,
          mapCoordinates: _currentCameraTarget,
          onSearchSuggestionTapped: (LatLng newCoordinates, String newAddress) {
            final event = MoveCameraToSelectedLocationEvent(coordinates: newCoordinates, address: newAddress);
            _mapPositionBloc.add(event);
          },
          onKeyboardAppearing: () => setState(() {
            _showAddressCard = false;
          }),
          onKeyboardDisappearing: () => setState(() {
            _showAddressCard = true;
          }),
        ));
  }

  Widget _buildPositionedAddressCard(MapPositionState state) {
    AddressCard card;

    if (state is MapPositionWithAddressPending) {
      card = AddressCard(mode: AddressCardMode.refreshing);
    } else if (state is MapPositionWithAddressFound) {
      card = AddressCard(
          mode: AddressCardMode.success,
          address: state.address,
          onConfirmation: () {
            final location = model.Location(coordinates: state.coordinates, address: state.address);
            widget.onSelected(location);
          });
    } else {
      card = AddressCard(mode: AddressCardMode.unavailable);
    }

    return Positioned(
        key: const Key("Address Card"),
        bottom: 15.0,
        child: Container(alignment: Alignment.center, padding: EdgeInsets.symmetric(horizontal: 20.0), child: card));
  }

  bool _areCoordinatesEqual(LatLng a, LatLng b) {
    return (a.latitude == b.latitude && a.longitude == b.longitude);
  }

  Future<void> _moveCameraToTarget(LatLng coordinates) async {
    _currentCameraTarget = coordinates;
    _controller.animateCamera(CameraUpdate.newLatLng(coordinates));
  }
}
