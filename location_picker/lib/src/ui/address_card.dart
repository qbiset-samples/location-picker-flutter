/*
 * Copyright (c) 2020 Quentin BISET. All rights reserved.
 */

import 'package:flutter/material.dart';

enum AddressCardMode { success, refreshing, unavailable }

class AddressCard extends StatelessWidget {
  /// Parameter _address_ and _onConfirmation_ must not be null if _mode_ == _success_
  AddressCard({@required this.mode, this.address, this.onConfirmation}) : super();

  final AddressCardMode mode;
  final String address;
  final void Function() onConfirmation;

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);

    Widget child;

    switch (mode) {
      case AddressCardMode.success:
        {
          child = Row(children: [
            Padding(
                key: Key("Address"),
                padding: EdgeInsets.only(left: 10.0),
                child: Text(address, maxLines: 3, style: themeData.textTheme.bodyText1)),
            IconButton(
                key: Key("Confirmation Button"),
                icon: Icon(Icons.check_circle_outline, color: themeData.buttonColor),
                onPressed: onConfirmation,
                iconSize: 30.0)
          ]);
        }
        break;
      case AddressCardMode.unavailable:
        {
          child = Center(
              child: Padding(
            padding: EdgeInsets.all(10.0),
            child: Text(
                "L'adresse correspondant à cet endroit n'a pas pu être trouvée. Vérifiez votre connexion internet si le problème persiste.",
                maxLines: 3,
                style: themeData.textTheme.bodyText1),
          ));
        }
        break;
      case AddressCardMode.refreshing:
        {
          child = Center(
              child: Padding(
                  padding: EdgeInsets.all(10.0), child: CircularProgressIndicator(backgroundColor: Colors.grey)));
        }
        break;
    }

    return Card(child: child);
  }
}
