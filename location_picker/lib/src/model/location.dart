/*
 * Copyright (c) 2020 Quentin BISET. All rights reserved.
 */

import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Location {
  const Location({@required this.coordinates, this.address});

  final LatLng coordinates;
  final String address;
}