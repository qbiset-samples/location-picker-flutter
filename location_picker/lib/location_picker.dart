library location_picker;

import 'package:flutter/material.dart';
import 'src/ui/concrete_location_picker.dart' as concrete;
import 'package:google_maps_flutter/google_maps_flutter.dart' show LatLng;
import 'src/model/location.dart';

export 'package:google_maps_flutter/google_maps_flutter.dart' show LatLng;
export 'src/model/location.dart';

class LocationPicker {
  LocationPicker._();

  static concrete.LocationPicker build(
      {Key key,
      @required String apiKey,
      @required LatLng initialCoordinates,
      @required String title,
      Function(Location) onSelected}) {
    return concrete.LocationPicker(
        key: key, apiKey: apiKey, initialCoordinates: initialCoordinates, title: title, onSelected: onSelected);
  }
}
