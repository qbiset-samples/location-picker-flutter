# Location Picker

Author: Quentin BISET  
Version: 1.0.0  
Platform: Flutter  
Design pattern: BLoC  
Language: Dart  
Packages: flutter_bloc, equatable, rxdart (to debounce events in the BLoC), google_maps_flutter, google_maps_webservice, geolocator, uuid

---
## What does this package do?
This package provides a ready-made widget allowing the user to select a location using a Google Map view and a search bar.
The user can move a Google Map around to select the location, or use a search bar and select one of the provided suggestions. The user must confirm the selection by tapping a checkmark button.

## How do I use this package?  
#### 1. Create the relevant Google API keys
Go to the [Google Cloud Platform](https://console.cloud.google.com/google/maps-apis/), create a project and 3 different API keys :
+ One key for **Google Maps SDK for Android**
+ One key for **Google Maps SDK for iOS**
+ One key for the webservices APIs (**Places**, **Geocoding**)

#### 2. Add Android metadata and permissions
Open your Android app's manifest (`android/app/src/main/AndroidManifest.xml`) and add the fine location permission to allow the location picker to start at the device's current location.
```xml
<manifest ...
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
```

Then provide the **Google Maps SDK for Android** API key to your manifest.
```xml
<manifest ...
  <application ...
    <meta-data android:name="com.google.android.geo.API_KEY"
               android:value="YOUR KEY HERE"/>
```

#### 3. Add iOS metadata and permissions
Open your iOS app's `Info.plist` and make sure you added the **NSLocationWhenInUseUsageDescription** key to allow the location picker to get the device's location.

Then provide the **Google Maps SDK for iOS** to your AppDelegate.
If your projet is in Objective-C, open `ios/Runner/AppDelegate.m`
```objectivec
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  [GMSServices provideAPIKey:@"YOUR KEY HERE"];
  [GeneratedPluginRegistrant registerWithRegistry:self];
  return [super application:application didFinishLaunchingWithOptions:launchOptions];
}
```

If your project is written in Swift, open `ios/Runner/AppDelegate.swift`
```swift
override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
{
    GMSServices.provideAPIKey("YOUR KEY HERE")
    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
}
```

#### 4. Build a location picker!
With the setup in place, you just need to import the package and use the provided static constructor to create a new location picker widget.
The `apiKey` parameter is for your third API key, which will be used to retrieve the address of the currently selected location (if the user changes it by moving the map), provide autocompletion suggestions when using the search bar or retrieve a suggestion's coordinates if the user selected it.
The `onSelected` parameter allows you to define a callback function, that will be called when the user confirms the desired location by tapping on a checkmark button.

```dart
class _WrappedLocationPicker extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LocationPicker.build(
        key: Key("Location Picker"),
        apiKey: "AIzaSyBrg0QKCQkWWepmu9MGl3woAsLdS-RRvwA",
        initialCoordinates: LatLng(50.6292, 3.0573),
        title: "Sélectionnez votre adresse de livraison",
        onSelected: (Location location) {
          ...
        });
  }
}
```

---
## Possible improvements
+ In some circumstances, the map controller can be called before it has been properly set. Look for an alternative to _Completer_ since we must access it more than once.
+ Provide a new constructor to pass the URL of a proxy instead of the webservices API key. Unlike the _Google Maps SDK_ API keys, the webservices API key cannot be restricted by application. As a result, it is safer to let a proxy inject the API key and restrict said API key with the proxy's IP address.
+ Create a custom data structure and add it as an optional parameter, to somewhat customize the widget's appearance (colors, maybe fonts?).
